﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Newtonsoft.Json;
using Npgsql;
using TranscationsCarService.Models;

namespace TranscationsCarService
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.RepeatableRead;

        public MainWindow()
        {

            InitializeComponent();
        }

        private Dictionary<string, string> _fuelTranslation = new Dictionary<string, string>()
        {
            {"System.Windows.Controls.ComboBoxItem: Бензин", "Gasoline"},
            {"System.Windows.Controls.ComboBoxItem: Дизель", "Diesel"},
            {"System.Windows.Controls.ComboBoxItem: Електрика", "Electricity"},
            {"System.Windows.Controls.ComboBoxItem: Бензин/Газ", "Gasoline/Propane"},
            {"System.Windows.Controls.ComboBoxItem: Гібрид", "Hybrid"}
        };

        private Dictionary<string, string> _aspirationTranslation = new Dictionary<string, string>()
        {
            {"System.Windows.Controls.ComboBoxItem: Атмосферний", "N/A"},
            {"System.Windows.Controls.ComboBoxItem: Турбонаддув", "TURBOCHARGED"},
        };

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var list = new List<Customer>();

            using var pgConnect = new NpgsqlConnection("Host=localhost;Port=5432;Database=CarService;Username=postgres;Password=1111");
            pgConnect.Open();

            using (var pgCommand = new NpgsqlCommand("SELECT customer_id, name, surname, phone_number, birth_date, gender FROM customer c JOIN person p ON p.person_id = c.person_id;", pgConnect))
            {
                using (var reader = pgCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        list.Add(new Customer()
                        {
                            id = int.Parse(reader["customer_id"].ToString() ?? string.Empty),
                            name = reader["name"].ToString(),
                            surname = reader["surname"].ToString(),
                            phone_number = reader["phone_number"].ToString(),
                            birth_date = DateOnly.Parse(reader["birth_date"].ToString()?.Substring(0, 10) ?? string.Empty),
                            gender = reader["gender"].ToString()
                        });
                    }
                }
            }

            DataGridView.Columns.Clear();

            DataGridView.ItemsSource = list;

            DataGridView.Columns.Remove(DataGridView.Columns.Last());

            pgConnect.Close();
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            using (var httpClient = new HttpClient())
            {
                using HttpResponseMessage responceMessage = await httpClient.GetAsync("https://localhost:7200/cars");

                responceMessage.EnsureSuccessStatusCode();

                var jsonResponce = await responceMessage.Content.ReadAsStringAsync();

                var cars = JsonConvert.DeserializeObject<List<Car>>(jsonResponce);

                DataGridView.Columns.Clear();

                DataGridView.ItemsSource = cars;
            }
        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            using (var httpClient = new HttpClient())
            {
                using HttpResponseMessage responceMessage = await httpClient.GetAsync("https://localhost:7200/customers");

                responceMessage.EnsureSuccessStatusCode();

                var jsonResponce = await responceMessage.Content.ReadAsStringAsync();

                var customersWithCars = JsonConvert.DeserializeObject<List<Customer>>(jsonResponce);

                DataGridView.Columns.Clear();

                DataGridView.ItemsSource = customersWithCars;

                DataGridView.Columns.Remove(DataGridView.Columns.Last());

                DataGridTextColumn textColumn = new DataGridTextColumn();

                textColumn.Header = "cars";
                textColumn.Binding = new Binding("cars[0]");

                DataGridView.Columns.Add(textColumn);

            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            StringBuilder requestString = new StringBuilder();
            requestString.Append("SELECT vin_code, brand_name, model_name, volume, fuel_type, aspiration FROM car" +
                " JOIN public.car_model cm on car.model_id = cm.model_id " +
                "JOIN public.engine e on e.engine_id = cm.engine_id");

            if (BrandBox.Text.Length > 0)
            {
                requestString.Append($" WHERE brand_name = '{BrandBox.Text}'");
            }
            if (ModelBox.Text.Length > 0)
            {
                if (requestString.Length == 185)
                {
                    requestString.Append($" WHERE model_name = '{ModelBox.Text}'");
                } else
                {
                    requestString.Append($" AND model_name = '{ModelBox.Text}'");
                }
            }
            if (VolumeBox.Text.Length > 0)
            {
                if (requestString.Length == 185)
                {
                    requestString.Append($" WHERE volume = {VolumeBox.Text}"); 
                } else
                {
                    requestString.Append($" AND volume = {VolumeBox.Text}");
                }
            }
            if (FuelTypeComboBox.SelectedIndex != 0)
            {
                if (requestString.Length == 185)
                {
                    requestString.Append($" WHERE fuel_type = '{_fuelTranslation.Where(item => item.Key.ToString().Equals(FuelTypeComboBox.SelectedValue.ToString())).First().Value}'");
                } else
                {

                    requestString.Append($" AND fuel_type = '{_fuelTranslation.Where(item => item.Key.ToString().Equals(FuelTypeComboBox.SelectedValue.ToString())).First().Value}'");
                }
            }
            if (AspirationComboBox.SelectedIndex != 0)
            {
                if (requestString.Length == 185)
                {
                    requestString.Append($" WHERE aspiration = '{_aspirationTranslation.Where(item => item.Key.ToString().Equals(AspirationComboBox.SelectedValue.ToString())).First().Value}'");
                }
                else
                {
                    requestString.Append($" AND aspiration = '{_aspirationTranslation.Where(item => item.Key.ToString().Equals(AspirationComboBox.SelectedValue.ToString())).First().Value}'");
                }
            }

            requestString.Append(";");

            var list = new List<Car>();

            using var pgConnect = new NpgsqlConnection("Host=localhost;Port=5432;Database=CarService;Username=postgres;Password=1111");
            pgConnect.Open();

            using (var pgCommand = new NpgsqlCommand(requestString.ToString(), pgConnect))
            {
                using (var reader = pgCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        list.Add(new Car()
                        {
                            vin_code = reader["vin_code"].ToString(),
                            brand_name = reader["brand_name"].ToString(),
                            model_name = reader["model_name"].ToString(),
                            engine_volume = int.Parse(reader["volume"].ToString()),
                            fuel_type = reader["fuel_type"].ToString(),
                            aspiration = reader["aspiration"].ToString()
                        });
                    }
                }
            }

            DataGridSelect.Columns.Clear();

            DataGridSelect.ItemsSource = list;

            pgConnect.Close();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            TextBlockExceptions.Text = string.Empty;
            NpgsqlTransaction transaction = null;

            using var pgConnect = new NpgsqlConnection("Host=localhost;Port=5432;Database=CarService;Username=postgres;Password=1111;IncludeErrorDetail=true;");
            pgConnect.Open();
            transaction = pgConnect.BeginTransaction();

            var pgCommand = new NpgsqlCommand("CALL add_customer_only(@name, @surname, @birthday, @gender, @phone_number);", pgConnect);

            try
            {
                pgCommand.Parameters.AddWithValue("@name", NameTextBox.Text);
                pgCommand.Parameters.AddWithValue("@surname", SurnameTextBox.Text);
                pgCommand.Parameters.AddWithValue("@birthday", DateOnly.Parse(BirthdayTextBox.Text));
                pgCommand.Parameters.AddWithValue("@gender", GenderTextBox.Text);
                pgCommand.Parameters.AddWithValue("@phone_number", PhoneNumberTextBox.Text);

                pgCommand.ExecuteNonQuery();
                transaction.Commit();

                TextBlockExceptions.Text = "Коритувача було успішно додано.";
            }
            catch(FormatException fEx)
            {
                TextBlockExceptions.Text += "\n Помилка: " + fEx.Message.ToString();
                transaction.Dispose();
            }
            catch(Exception ex)
            {
                TextBlockExceptions.Text += "\n Помилка: " + ex.Message.ToString();
                transaction.Rollback();
                try
                {
                    transaction = pgConnect.BeginTransaction();
                    TextBlockExceptions.Text += "\n\n Повторна спроба виконання транзакції.";
                    pgCommand.ExecuteNonQuery();
                    transaction.Commit();
                } catch(Exception excep)
                {
                    TextBlockExceptions.Text += "\n\n Помилка: " + excep.Message.ToString();
                    transaction.Rollback();
                }
            }
            finally
            {
                pgConnect.Close();
            }
            
        }

        private async void Button_Click_5(object sender, RoutedEventArgs e)
        {
            ConcurrentTransactions.Text = string.Empty;

            try
            {

                Task b = PerformAsyncUpdate();
                Task a = PerformAsyncDelete();
                

                await Task.WhenAll(a, b);
            }
            catch (Exception ex)
            {
                ConcurrentTransactions.Text += "Main: " + ex.ToString();
            }
        }
        

        private async Task PerformAsyncDelete()
        {
            NpgsqlTransaction npgsqlTransaction = null;
            using var pgConnect = new NpgsqlConnection("Host=localhost;Port=5432;Database=CarService;Username=postgres;Password=1111;IncludeErrorDetail=true;");
            pgConnect.Open();

            npgsqlTransaction = await pgConnect.BeginTransactionAsync(isolationLevel);

            try
            {
                var read = new NpgsqlCommand("SELECT * FROM order_item WHERE order_id = 9", pgConnect);

                using var reader = await read.ExecuteReaderAsync();

                var list = new List<object>();

                while (reader.Read())
                {
                    list.Add(new { order_id = reader["order_id"], service_id = reader["service_id"], employee_id = reader["employee_id"] });
                }
                await reader.DisposeAsync();

                ConcurrentTransactions.Text += ("\nДані до видалення:\n");
                list.ForEach(item => ConcurrentTransactions.Text += "\n" + (item));

                var pgCommand = new NpgsqlCommand("DELETE FROM order_item WHERE order_id = 9 AND service_id = 30;", pgConnect);

                var result = await pgCommand.ExecuteNonQueryAsync();

                ConcurrentTransactions.Text += ("\nРядків видалено: " + result);

                var read2 = new NpgsqlCommand("SELECT * FROM order_item WHERE order_id = 9", pgConnect);

                using var reader2 = await read2.ExecuteReaderAsync();

                var list2 = new List<object>();

                while (reader2.Read())
                {
                    list2.Add(new { order_id = reader["order_id"], service_id = reader["service_id"], employee_id = reader["employee_id"] });
                }
                await reader2.DisposeAsync();
                ConcurrentTransactions.Text += ("\nДані після видалення:\n");
                list2.ForEach(item => ConcurrentTransactions.Text += "\n" + (item));
                await npgsqlTransaction.CommitAsync();

                ConcurrentTransactions.Text += ("\nВидалення відбулось.");
            }
            catch (Exception excep)
            {
                ConcurrentTransactions.Text += ("\nПомилка на видаленні: " + excep.Message);
                npgsqlTransaction.Rollback();

                try
                {
                    NpgsqlTransaction repeatDelete = await pgConnect.BeginTransactionAsync(isolationLevel);

                    var deleteCommand = new NpgsqlCommand("DELETE FROM order_item WHERE order_id = 9 AND service_id = 30;", pgConnect);

                    int result = await deleteCommand.ExecuteNonQueryAsync();

                    if (result == 0)
                    {
                        ConcurrentTransactions.Text += ("\nДруга спроба видалення. Нічого не було видалено, оскільки рядка з такими параметрами не існує.");
                    }

                    await repeatDelete.CommitAsync();
                }
                catch (Exception excep2)
                {
                    ConcurrentTransactions.Text += ("\nДруга спроба видалення. Помилка: " + excep2.Message);
                }
            }
        }

        private async Task PerformAsyncUpdate()
        {
            NpgsqlTransaction npgsqlTransaction = null;
            using var pgConnect = new NpgsqlConnection("Host=localhost;Port=5432;Database=CarService;Username=postgres;Password=1111;IncludeErrorDetail=true;");
            pgConnect.Open();

            npgsqlTransaction = await pgConnect.BeginTransactionAsync(isolationLevel);
            try
            {
                var read = new NpgsqlCommand("SELECT * FROM order_item WHERE order_id = 9", pgConnect);

                using var reader = await read.ExecuteReaderAsync();

                var list = new List<object>();

                while (reader.Read())
                {
                    list.Add(new { order_id = reader["order_id"], service_id = reader["service_id"], employee_id = reader["employee_id"] });
                }
                await reader.DisposeAsync();

                ConcurrentTransactions.Text += ("\nДані до оновлення:\n");
                list.ForEach(item => ConcurrentTransactions.Text += "\n" + (item));

                var pgCommand = new NpgsqlCommand("UPDATE order_item SET service_id = 10 WHERE order_id = 9 AND service_id = 30 AND employee_id = 7;", pgConnect);

                var result = await pgCommand.ExecuteNonQueryAsync();


                ConcurrentTransactions.Text += ("\nРядків оновлено: " + result);

                var read2 = new NpgsqlCommand("SELECT * FROM order_item WHERE order_id = 9", pgConnect);

                using var reader2 = await read2.ExecuteReaderAsync();



                var list2 = new List<object>();

                while (reader2.Read())
                {
                    list2.Add(new { order_id = reader["order_id"], service_id = reader["service_id"], employee_id = reader["employee_id"] });
                }
                await reader2.DisposeAsync();

                ConcurrentTransactions.Text += ("\nДані після оновлення:\n");
                list2.ForEach(item => ConcurrentTransactions.Text += "\n" + (item));

                await npgsqlTransaction.CommitAsync();
                ConcurrentTransactions.Text += ("\nОновлення відбулось.\n");
            }
            catch (Exception excep)
            {
                ConcurrentTransactions.Text += ("\nПомилка на оновленні: " + excep.Message);
                await npgsqlTransaction.RollbackAsync();
                try
                {
                    NpgsqlTransaction insertTransaction = await pgConnect.BeginTransactionAsync(isolationLevel);

                    var command = new NpgsqlCommand("INSERT INTO order_item(order_id, service_id, employee_id)VALUES(9, 10, 7);", pgConnect);

                    command.ExecuteNonQuery();

                    await insertTransaction.CommitAsync();

                    ConcurrentTransactions.Text += ("\nНовий рядок з апдейту було успішно додано.");
                }
                catch (Exception excep2)
                {
                    ConcurrentTransactions.Text += ("\nДодавання після невдалого апдейту не відбулось: " + excep2.Message);
                }
            }
        }
    }
}
