﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscationsCarService.Models
{
    public class Car
    {
        public string? vin_code { get; set; }

        public string? brand_name { get; set; }

        public string? model_name { get; set; }

        public int engine_volume { get; set; }

        public string? fuel_type { get; set; }

        public string? aspiration { get; set; }

        public override  string ToString()
        {
            return $"{brand_name} {model_name} {engine_volume} {vin_code} {fuel_type} {aspiration}";
        }
    }
}
