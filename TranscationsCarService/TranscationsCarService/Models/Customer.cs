﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranscationsCarService.Models
{
    class Customer
    {
        public int id { get; set; }

        public string? name { get; set; }

        public string? surname { get; set; }

        public string? gender { get; set; }

        public DateOnly birth_date { get; set; }

        public string? phone_number { get; set; }

        public ObservableCollection<Car>? cars { get; set; }
        
        
    }
}
